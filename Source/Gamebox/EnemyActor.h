// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "Weapon/WeaponHandleInterface/WeaponHandleInterface.h"

#include "EnemyActor.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOX_API AEnemyActor : public ADefaultPawn, public IWeaponHandleInterface
{
	GENERATED_BODY()
    
public:
    AEnemyActor();

    virtual void PickUpWeapon(AWeapon* Weapon) override;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* Body;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* Head;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* Muzzle;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class USceneComponent* MuzzleLocation;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Weapon, meta = (AllowPrivateAccess = "true"))
    TSubclassOf<AWeapon> DefaultWeapon;

protected:
    virtual void BeginPlay() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Score)
    int32 ScorePoints; 
};
