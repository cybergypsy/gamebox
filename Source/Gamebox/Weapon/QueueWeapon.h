// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "QueueWeapon.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOX_API AQueueWeapon : public AWeapon
{
	GENERATED_BODY()

protected:
    virtual void FireInstance() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon, meta = (AllowPrivateAccess = "true"))
    float QueueRate;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon, meta = (AllowPrivateAccess = "true"))
    float QueueSize;
    
    FTimerHandle QueueTimer;

    int32 CurrentQueueShot;
    bool InQueue;

};
