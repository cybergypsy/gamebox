// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponHandleInterface.h"

#include "GameFramework/DefaultPawn.h"

void IWeaponHandleInterface::SetCurrentWeapon(AWeapon* NewWeapon)
{
    MyWeapon = NewWeapon;
    MyWeapon->SetInstigator(Cast<ADefaultPawn>(this));
}

AWeapon* IWeaponHandleInterface::GetCurrentWeapon() const
{
    return MyWeapon;
}
