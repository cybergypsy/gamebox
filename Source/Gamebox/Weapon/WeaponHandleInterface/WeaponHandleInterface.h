// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gamebox/Weapon/Weapon.h"
#include "UObject/Interface.h"
#include "WeaponHandleInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, meta=(CannotImplementInterfaceInBlueprint))
class UWeaponHandleInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GAMEBOX_API IWeaponHandleInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	virtual void SetCurrentWeapon(AWeapon* NewWeapon);
	
	UFUNCTION(BlueprintCallable)
    virtual AWeapon* GetCurrentWeapon() const;

	UFUNCTION(BlueprintCallable)
	virtual void PickUpWeapon(AWeapon* Weapon) = 0; // what needs to be done before setting currentWeapon

private:
	class AWeapon* MyWeapon = nullptr; // create this outside
};
