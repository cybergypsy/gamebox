// Fill out your copyright notice in the Description page of Project Settings.


#include "QueueWeapon.h"
#include "Engine/Engine.h"
#include "TimerManager.h"

void AQueueWeapon::FireInstance()
{
    if (CurrentAmmoCount == 0)
        return;
    
    CurrentAmmoCount--;
    if (GetWorld()->GetTimerManager().GetTimerRemaining(QueueTimer) > 0)
        return;
    const bool ItsNewQueue = !GetWorld()->GetTimerManager().IsTimerActive(ShotTimer)
                                && !GetWorld()->GetTimerManager().IsTimerActive(QueueTimer)
                                && CurrentQueueShot == 0;
    if (ItsNewQueue) // if player call void Fire() and no shots in queue left
    {
        GetWorld()->GetTimerManager().SetTimer(ShotTimer, this, &AQueueWeapon::EnableFire, WeaponRate, false);
        InQueue = true;
        CurrentQueueShot = QueueSize;
    }
    
     GetWorld()->SpawnActor<AProjectile>(Projectile, MuzzleLocation->GetComponentLocation(), GetActorRotation());
    //set another shots in queue
    if (--CurrentQueueShot)
        GetWorld()->GetTimerManager().SetTimer(QueueTimer, this, &AQueueWeapon::FireInstance, QueueRate, false);
    else
        InQueue = false;
    if (CurrentAmmoCount == 0 && !EnableAutoReloadAmmo) //if ammo doesn't exist - Destroy weapon
    {
        Destroy();
    }
    FireSignal.Broadcast();
}
