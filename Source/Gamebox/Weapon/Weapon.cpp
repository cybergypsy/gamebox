// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Components/StaticMeshComponent.h"
#include "Components/AudioComponent.h"
#include "Components/SphereComponent.h"
#include "Projectile.h"
#include "TimerManager.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "UObject/ConstructorHelpers.h"

void AWeapon::RefillAmmoOnTimer()
{
	PickUpAmmo(1);
}

// Sets default values
AWeapon::AWeapon() : WeaponRate(1.f),  CanShoot(true)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionProfileName(FName("OverlapAllDynamic"));

	MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Muzzle"));
	MuzzleLocation->SetupAttachment(Mesh);

	static ConstructorHelpers::FObjectFinder<USoundCue> Explosion(
	TEXT("SoundCue'/Game/StarterContent/Audio/Explosion_Cue'"));
	SoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Sound"));
	SoundComponent->SetupAttachment(MuzzleLocation);
	SoundComponent->SetSound(ExplosionCue);

}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	if (EnableAutoReloadAmmo)
		GetWorld()->GetTimerManager().SetTimer(ReloadTimer, this, &AWeapon::RefillAmmoOnTimer, RefillingAmmoRate, true);
}

void AWeapon::EnableFire()
{
	CanShoot = true;
}

void AWeapon::FireInstance()
{
	if (CurrentAmmoCount == 0)
		return;
	CurrentAmmoCount--;
	GetWorld()->GetTimerManager().SetTimer(ShotTimer, this, &AWeapon::EnableFire, WeaponRate, false);
	auto SpawnedProjectile = GetWorld()->SpawnActor<AProjectile>(Projectile, MuzzleLocation->GetComponentLocation(), GetActorRotation());
	if (IsValid(SpawnedProjectile))
		SpawnedProjectile->SetInstigator(this->GetInstigator());
	FireSignal.Broadcast();
}

void AWeapon::Fire()
{
	if (!CanShoot)
	{
		return;
	}

	CanShoot = false;
	FireInstance();
	//SoundComponent->Play(2.f); //TODO isn't working
	UGameplayStatics::PlaySound2D(GetWorld(), SoundComponent->Sound);
}

void AWeapon::PickUpAmmo(int32 PickedAmmoCount)
{
	if (MaxAmmo == -1 || CurrentAmmoCount + PickedAmmoCount <= MaxAmmo)
	{
		CurrentAmmoCount += PickedAmmoCount;
	}
	else if (PickedAmmoCount + CurrentAmmoCount > MaxAmmo && CurrentAmmoCount <= MaxAmmo)
	{
		CurrentAmmoCount = MaxAmmo;
	}
}

