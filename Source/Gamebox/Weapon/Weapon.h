// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFire);

UCLASS()
class GAMEBOX_API AWeapon : public AActor
{
	GENERATED_BODY()

	void RefillAmmoOnTimer();
	
public:	
	// Sets default values for this actor's properties
	AWeapon();
	
	UFUNCTION(BlueprintCallable)
    void Fire(); //call if you need to shoot something 

	UFUNCTION(BlueprintCallable)
	void PickUpAmmo(int32 PickedAmmoCount = 1);
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon, meta = (AllowPrivateAccess = "true"))
    int32 WeaponRate; //how long it will take before enable func Fire();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo, meta = (AllowPrivateAccess = "true"))
    TSubclassOf<AProjectile> Projectile;

	UPROPERTY(BlueprintAssignable, Category = EventDispatchers)
	FOnFire FireSignal;
	
protected:
	FTimerHandle ShotTimer; // timer to wait before another shot
	FTimerHandle ReloadTimer; // timer to refill ammo count
	class USoundCue* ExplosionCue;
	bool CanShoot;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon, meta = (AllowPrivateAccess = "true"))
    class USceneComponent* MuzzleLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon, meta = (AllowPrivateAccess = "true"))
	class UAudioComponent* SoundComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Ammo, meta = (AllowPrivateAccess = "true"))
	int32 MaxAmmo; //set -1 if infinite

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Ammo, meta = (AllowPrivateAccess = "true"))
	int32 CurrentAmmoCount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Ammo, meta = (AllowPrivateAccess = "true"))
	bool EnableAutoReloadAmmo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Ammo, meta = (AllowPrivateAccess = "true"))
	float RefillingAmmoRate; // how long it will takes until will added ammo;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	void EnableFire();

	/*override if you need different fire logic*/
	virtual void FireInstance();
};
