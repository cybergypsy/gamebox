// Fill out your copyright notice in the Description page of Project Settings.


#include "Hovercraft.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"

// Sets default values
AHovercraft::AHovercraft()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	Body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	Body->SetupAttachment(RootComponent);
	Body->SetEnableGravity(true);
	Body->SetSimulatePhysics(true);

	Head = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Head"));
	Head->SetupAttachment(Body);
	
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	Arrow->SetupAttachment(Head);
	
	Muzzle = CreateDefaultSubobject<USceneComponent>(TEXT("Muzzle"));
	Muzzle->SetupAttachment(Arrow);
}

// Called when the game starts or when spawned
void AHovercraft::BeginPlay()
{
	Super::BeginPlay();
	PickUpWeapon(Cast<AWeapon>(GetWorld()->SpawnActor<AWeapon>(WeaponSpawn)));
}

// Called every frame
void AHovercraft::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AHovercraft::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AHovercraft::PickUpWeapon(AWeapon* Weapon)
{
	if(!IsValid(Weapon))
		return;

	Weapon->AttachToComponent(Muzzle, FAttachmentTransformRules::SnapToTargetIncludingScale);
	if (IsValid(GetCurrentWeapon()))
	{
		GetCurrentWeapon()->OnDestroyed.RemoveAll(this);
		GetCurrentWeapon()->Destroy();
	}
	SetCurrentWeapon(Weapon);

	
	/* TODO
	* 	CurrentWeapon->OnDestroyed.AddDynamic(this, ...);
	* 	CurrentWeapon->FireSignal.AddDynamic(this, CameraShake);
	*/
}

